package com.soup;

import com.soup.util.http.TrustAllTrustManager;
import com.vmware.vim25.AboutInfo;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ServiceContent;
import com.vmware.vim25.VimPortType;
import com.vmware.vim25.VimService;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.ws.BindingProvider;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author zhaoyi
 * @date 2018-12-22 13:44
 */
public class ServiceInstanceTest {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected String url = "https://172.50.11.10/sdk";
    protected String userName = "administrator@vsphere.local";
    protected String password = "P@ssw0rd";

    protected VimPortType vimPort;
    protected ServiceContent serviceContent;

    protected ManagedObjectReference viewMgrRef;
    protected ManagedObjectReference propColl;

    @Before
    public void connect() throws Exception {
        TrustAllTrustManager.trustAllHttpsCertificates();
        HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);

        ManagedObjectReference serviceInstanceMor = new ManagedObjectReference();
        String SVC_INST_NAME = "ServiceInstance";
        serviceInstanceMor.setType(SVC_INST_NAME);
        serviceInstanceMor.setValue(SVC_INST_NAME);

        VimService vimService = new VimService();
        vimPort = vimService.getVimPort();

        BindingProvider provider = (BindingProvider) vimPort;
        Map<String, Object> requestContext = provider.getRequestContext();

        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        requestContext.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);

        serviceContent = vimPort.retrieveServiceContent(serviceInstanceMor);
        vimPort.login(serviceContent.getSessionManager(), userName, password, null);

        viewMgrRef = serviceContent.getViewManager();
        propColl = serviceContent.getPropertyCollector();

        AboutInfo aboutInfo = serviceContent.getAbout();
        logger.info("connect vc[{}] success, fullNmae[{}], apiVerson[{}]", url, aboutInfo.getFullName(),
                        aboutInfo.getApiVersion());
    }

    @After
    public void logout() throws Exception {
        vimPort.logout(serviceContent.getSessionManager());
        logger.info("logout success!");
    }
}
