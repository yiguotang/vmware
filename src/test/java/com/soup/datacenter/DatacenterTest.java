package com.soup.datacenter;

import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RetrieveOptions;
import com.vmware.vim25.RetrieveResult;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.ServiceContent;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.VimPortType;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 * Description: 数据中心的测试类
 * </p>
 *
 * @author zhaoyi
 * @date 2018-12-23 21:39
 */
public class DatacenterTest {

    private static TraversalSpec getDatacenterTraversalSpec() {
        SelectionSpec selectionSpec = new SelectionSpec();
        selectionSpec.setName("VisitFolders");

        TraversalSpec traversalSpec = new TraversalSpec();
        traversalSpec.setName("VisitFolders");
        traversalSpec.setType("Folder");
        traversalSpec.setPath("childEntity");
        traversalSpec.setSkip(Boolean.FALSE);
        traversalSpec.getSelectSet().add(selectionSpec);

        return traversalSpec;
    }

    private static List<ObjectContent> retrievePropertiesAllObjects(ServiceContent serviceContent, VimPortType vimPort,
                    List<PropertyFilterSpec> filterSpecs) {
        RetrieveOptions retrieveOptions = new RetrieveOptions();
        List<ObjectContent> objectContentList = Lists.newArrayList();

        try {
            RetrieveResult retrieveResult =
                            vimPort.retrievePropertiesEx(serviceContent.getPropertyCollector(), filterSpecs,
                                            retrieveOptions);
            if (null != retrieveResult && !CollectionUtils.isEmpty(retrieveResult.getObjects())) {
                objectContentList.addAll(retrieveResult.getObjects());
            }

            String token = null;
            if (null != retrieveResult && StringUtils.isNotEmpty(retrieveResult.getToken())) {
                token = retrieveResult.getToken();
            }
            while (StringUtils.isNotEmpty(token)) {
                retrieveResult = vimPort.continueRetrievePropertiesEx(serviceContent.getPropertyCollector(), token);
                token = null;
                if (null != retrieveResult) {
                    token = retrieveResult.getToken();
                    if (!CollectionUtils.isEmpty(retrieveResult.getObjects())) {
                        objectContentList.addAll(retrieveResult.getObjects());
                    }
                }
            }
        } catch (InvalidPropertyFaultMsg | RuntimeFaultFaultMsg invalidPropertyFaultMsg) {
            invalidPropertyFaultMsg.printStackTrace();
        }

        return objectContentList;
    }

    /**
     * 根据数据中心的名称查询数据中心的mor对象
     *
     * @param serviceContent 连接对象
     * @param vimPort VimPortType
     * @param datacenterName 数据中心名称
     * @return ManagedObjectReference
     */
    private static ManagedObjectReference getDatacenterByName(ServiceContent serviceContent, VimPortType vimPort,
                    String datacenterName) {
        ManagedObjectReference datacenteMor = null;
        ManagedObjectReference rootFolder = serviceContent.getRootFolder();
        try {
            TraversalSpec traversalSpec = getDatacenterTraversalSpec();
            PropertySpec propertySpec = new PropertySpec();
            propertySpec.setAll(Boolean.FALSE);
            propertySpec.getPathSet().add("name");
            propertySpec.setType("Datacenter");

            ObjectSpec objectSpec = new ObjectSpec();
            objectSpec.setObj(rootFolder);
            objectSpec.setSkip(Boolean.TRUE);
            objectSpec.getSelectSet().add(traversalSpec);

            PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
            propertyFilterSpec.getPropSet().add(propertySpec);
            propertyFilterSpec.getObjectSet().add(objectSpec);

            List<PropertyFilterSpec> filterSpecs = Lists.newArrayList();
            filterSpecs.add(propertyFilterSpec);
            List<ObjectContent> objectContentList = retrievePropertiesAllObjects(serviceContent, vimPort, filterSpecs);
            if (!CollectionUtils.isEmpty(objectContentList)) {
                ManagedObjectReference mor;
                List<DynamicProperty> dps;
                for (ObjectContent objectContent : objectContentList) {
                    mor = objectContent.getObj();
                    dps = objectContent.getPropSet();
                    if (!CollectionUtils.isEmpty(dps)) {
                        for (DynamicProperty dp : dps) {
                            if (StringUtils.equals((String) dp.getVal(), datacenterName)) {
                                datacenteMor = mor;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datacenteMor;
    }
}
