package com.soup.vm;

import com.soup.ServiceInstanceTest;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RetrieveOptions;
import com.vmware.vim25.RetrieveResult;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Description: 虚拟机的测试类
 * </p>
 *
 * @author zhaoyi
 * @date 2018-12-23 22:44
 */
public class VmTest extends ServiceInstanceTest {

    @Test
    public void queryAllVm() throws Exception {
        List<String> vmList = Lists.newArrayList("VirtualMachine");
        ManagedObjectReference cViewRef = vimPort.createContainerView(viewMgrRef, serviceContent.getRootFolder(),
                        vmList, Boolean.TRUE);

        ObjectSpec oSpec = new ObjectSpec();
        oSpec.setObj(cViewRef);
        oSpec.setSkip(Boolean.TRUE);

        TraversalSpec tSpec = new TraversalSpec();
        tSpec.setName("traverseEntities");
        tSpec.setPath("view");
        tSpec.setSkip(Boolean.FALSE);
        tSpec.setType("ContainerView");

        oSpec.getSelectSet().add(tSpec);

        PropertySpec pSpec = new PropertySpec();
        pSpec.setType("VirtualMachine");
        pSpec.getPathSet().add("name");
        // 查询所有属性
        // pSpec.setAll(Boolean.TRUE);

        PropertyFilterSpec fSpec = new PropertyFilterSpec();
        fSpec.getObjectSet().add(oSpec);
        fSpec.getPropSet().add(pSpec);

        List<PropertyFilterSpec> fSpecList = Lists.newArrayList(fSpec);

        RetrieveOptions ro = new RetrieveOptions();
        RetrieveResult props = vimPort.retrievePropertiesEx(propColl, fSpecList, ro);

        if (props != null) {
            for (ObjectContent oc : props.getObjects()) {
                String vmName;
                String path;
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null) {
                    for (DynamicProperty dp : dps) {
                        vmName = (String) dp.getVal();
                        if (StringUtils.equals("88312722book1", vmName)) {
                            System.out.println(vmName);
                        }
                    }
                }
            }
        }
    }

    /**
     * 根据虚机的mor查询对象
     */
    @Test
    public void queryByMor() throws Exception {
        ManagedObjectReference mor = new ManagedObjectReference();
        mor.setType("VirtualMachine");
        mor.setValue("vm-20");

        // 遍历属性规范
        PropertySpec propSpec = new PropertySpec();
        propSpec.setAll(Boolean.FALSE);
        propSpec.getPathSet().add("name");
        propSpec.setType(mor.getType());

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(mor);
        objSpec.setSkip(Boolean.FALSE);

        // 属性过滤规范
        PropertyFilterSpec spec = new PropertyFilterSpec();
        spec.getPropSet().add(propSpec);
        spec.getObjectSet().add(objSpec);

        ArrayList<PropertyFilterSpec> listpfs = Lists.newArrayList(spec);

        List<ObjectContent> listobjcont = retrievePropertiesAllObjects(listpfs);
        if (listobjcont != null) {
            ObjectContent oc = listobjcont.get(0);
            logger.info("name: {}", oc.getPropSet().get(0).getVal());
        }
    }

    /**
     * 根据属性检索要查询的对象信息
     */
    private List<ObjectContent> retrievePropertiesAllObjects(List<PropertyFilterSpec> listpfs) throws Exception {
        RetrieveOptions propObjectRetrieveOpts = new RetrieveOptions();
        List<ObjectContent> listobjcontent = Lists.newArrayList();
        try {
            // 检索属性
            RetrieveResult rslts =
                            vimPort.retrievePropertiesEx(serviceContent.getPropertyCollector(), listpfs, propObjectRetrieveOpts);
            if (rslts != null && rslts.getObjects() != null && !rslts.getObjects().isEmpty()) {
                listobjcontent.addAll(rslts.getObjects());
            }
            String token = null;
            if (rslts != null && rslts.getToken() != null) {
                token = rslts.getToken();
            }
            while (token != null && !token.isEmpty()) {
                rslts = vimPort.continueRetrievePropertiesEx(serviceContent.getPropertyCollector(), token);
                token = null;
                if (rslts != null) {
                    token = rslts.getToken();
                    if (rslts.getObjects() != null && !rslts.getObjects().isEmpty()) {
                        listobjcontent.addAll(rslts.getObjects());
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return listobjcontent;
    }

    private static TraversalSpec getVmTraversalSpec() {
        TraversalSpec vAppToVm = new TraversalSpec();
        vAppToVm.setName("vAppToVm");
        vAppToVm.setType("VirtualApp");
        vAppToVm.setPath("vm");

        TraversalSpec vAppToVApp = new TraversalSpec();
        vAppToVApp.setName("vAppToVApp");
        vAppToVApp.setType("VirtualApp");
        vAppToVApp.setPath("resourcePool");

        SelectionSpec vAppRecursion = new SelectionSpec();
        vAppRecursion.setName("vAppToVApp");

        SelectionSpec vmInVApp = new SelectionSpec();
        vmInVApp.setName("vAppToVM");

        List<SelectionSpec> vAppToVMSS = Lists.newArrayList();
        vAppToVMSS.add(vAppRecursion);
        vAppToVMSS.add(vmInVApp);
        vAppToVApp.getSelectSet().addAll(vAppToVMSS);

        SelectionSpec selectionSpec = new SelectionSpec();
        selectionSpec.setName("VisitFolders");

        TraversalSpec datacenterToVmFolder = new TraversalSpec();
        datacenterToVmFolder.setName("DataCenterToVMFolder");
        datacenterToVmFolder.setType("Datacenter");
        datacenterToVmFolder.setPath("vmFolder");
        datacenterToVmFolder.setSkip(Boolean.FALSE);
        datacenterToVmFolder.getSelectSet().add(selectionSpec);

        TraversalSpec traversalSpec = new TraversalSpec();
        traversalSpec.setName("VisitFolders");
        traversalSpec.setType("Folder");
        traversalSpec.setPath("childEntity");
        traversalSpec.setSkip(Boolean.FALSE);
        List<SelectionSpec> selectionSpecArr = Lists.newArrayList();
        selectionSpecArr.add(selectionSpec);
        selectionSpecArr.add(datacenterToVmFolder);
        selectionSpecArr.add(vAppToVm);
        selectionSpecArr.add(vAppToVApp);
        traversalSpec.getSelectSet().addAll(selectionSpecArr);

        return traversalSpec;
    }
}
