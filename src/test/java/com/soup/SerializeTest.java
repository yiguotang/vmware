package com.soup;

import com.soup.util.SerializeUtil;
import lombok.Data;
import lombok.ToString;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author zhaoyi
 * @date 2019-01-02 17:00
 */
public class SerializeTest {

    @Test
    public void testSerialize() {
        User user = new User();
        user.setUserId(123456);
        user.setAddress("I am a good boy");
        user.setNote("this is test");
        List<String> list = Lists.newArrayList();
        list.add("record1");
        list.add("record2");
        list.add("record3");
        user.setRecords(list);
        Teacher teacher1 = new Teacher();
        teacher1.setName("语文老师");
        Teacher teacher2 = new Teacher();
        teacher2.setName("数学老师");
        List<Teacher> teachers = Lists.newArrayList();
        teachers.add(teacher1);
        teachers.add(teacher2);
        user.setTeachers(teachers);
        byte[] b = SerializeUtil.serialize(user);
        User rst = SerializeUtil.deserialize(b);
        System.out.println(rst);
    }

    @Data
    @ToString
    class User {
        private Integer userId;
        private String address;
        private String note;
        private List<String> records;
        private List<Teacher> teachers;
    }

    @Data
    class Teacher {
        private String name;
    }
}
